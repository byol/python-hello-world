# Hello world demo app with python back-end


- The internal PyPi registry is http://nexus.wdf.sap.corp:8081/nexus/content/groups/build.milestones.pypi/simple/
- For JWT parse and validation `sap_xssec` is used
- For HANA connectivity `hdbcli` package is used.


### 1. Change hosts in manifest

__CF deployment:__ Open `manifest.yml` and change `[c/d/i-user]`, so the hostnames of the deployed applications have uniques hostnames when deployed

__XSA deployment:__ Open `manifest-op.yml` and change `[hostname]`


### 2. Install dependencies

__Note__:
- You should have `npm` installed locally [https://nodejs.org/en/download/](https://nodejs.org/en/download/).
- To be able to resolve all dependencies, it is important to use nexus as your npm registry. You can check your current value using npm config get registry. To use nexus, type npm config set registry http://nexus.wdf.sap.corp:8081/nexus/content/groups/build.releases.npm/ -g

```sh
cd db
npm install
cd ../web
npm install
```

For the python back-end application it's important to download the dependencies for the platform where the application will be running. This is required because the SAP HANA python client lib `hdbcli` has platform specific distros. Running `pip download` command on mac for example will download the mac specific wheel that will fail if later deployed on SLES for example.
The python application is configured to run with Python 3.6.x in [runtime.txt](python/runtime.txt)

In case you are downloading dependencies on the same platform execute:

```sh
cd ../python
mkdir -p vendor
python3 -m pip download -d vendor -r requirements.txt --index-url http://nexus.wdf.sap.corp:8081/nexus/content/groups/build.milestones.pypi/simple/ --trusted-host nexus.wdf.sap.corp
```

Otherwise you can use the [pip-download.py](https://github.wdf.sap.corp/xs2-samples/python-hello-world/blob/master/pip-download.py) script with the correct Python version. Before using it check the code and adapt if required.

Example usage - from the root of the project execute:
```sh
python3 pip-download.py
```

### 3. Create services

#### SCP
```sh
cf create-service hana hdi-shared python-hdi-container
cf create-service xsuaa application python-uaa -c xs-security.json
```

#### XSA
```sh
xs create-service hana hdi-shared python-hdi-container
xs create-service xsuaa default python-uaa -c xs-security.json
```


### 4. Push applications

#### SCP
```sh
cf push python-hello-world-db
cf push python-hello-world-backend
cf push python-hello-world
```

#### XSA
```sh
xs push python-hello-world-db -f  manifest-op.yml
xs push python-hello-world-backend  -f manifest-op.yml
xs push python-hello-world  -f manifest-op.yml
```


### 5. Open the UI

  https://[i-number]-python-hello-world.cfapps.sap.hana.ondemand.com/index.html

  - in developer tools can be seen that the user info is loaded from python back-end
  - calling the user info service directly should result in 403 Unauthorized response:

  https://[i-number]-python-hello-world-backend.cfapps.sap.hana.ondemand.com/rest/addressbook/userinfo

### Deployment of mtar

  In order to use deployment of mtar, installation of MTA plugin and MTA builder is needed:
  - MTA plugin - [link](https://github.com/SAP/cf-mta-plugin)
  - MTA builder - [link](https://wiki.wdf.sap.corp/wiki/display/CXP/MTA+Build+Tool)

  ```sh
  java -jar mta_archive_builder.jar --build-target=XSA/CF build
  cf/xs deploy python-hello-world.mtar
  ```
