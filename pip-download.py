
import pip

pip.pep425tags.get_platform = lambda: 'linux_x86_64'
pip.main(['download',
    '-r', 'python/requirements.txt',
    '-d', 'python/vendor',
    '--index-url', 'http://nexus.wdf.sap.corp:8081/nexus/content/groups/build.milestones.pypi/simple/',
    '--trusted-host', 'nexus.wdf.sap.corp'])
