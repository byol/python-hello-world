""" Flask based web app exposing addressbook REST API's """
import os
import traceback
from flask import Flask
from flask import request
from flask import jsonify
from flask import g
import check
import tokenparser

from cfenv import AppEnv
from db import userinfo
from db import book
from db import tree

# pylint: disable=missing-docstring,protected-access

ENV = AppEnv()
UAA_CREDENTIALS = ENV.get_service(label='xsuaa').credentials
HANA_CREDENTIALS = ENV.get_service(label='hana').credentials

app = Flask(__name__) # pylint: disable=invalid-name

@app.before_request
def before_request():
    g._uaa_credentials = UAA_CREDENTIALS

@app.errorhandler(500)
def handle_error(err):
    traceback.print_exc()
    return jsonify({'error': str(err)}), 500

@app.route('/rest/addressbook/userinfo')
@check.authenticated
def fetch_user_info():
    user_info, raw_token = tokenparser.fetch_user_token(request)
    dbinfo = userinfo.set_and_fetch_db_user(HANA_CREDENTIALS, raw_token)

    user = {
        'id': user_info.get('user_name', ''),
        'name': {
            'givenName': user_info.get('given_name', ''),
            'familyName': user_info.get('family_name', '')
        },
        'emails': [{
            'value': user_info.get('email', '')
        }]
    }

    return jsonify({
        'hdbCurrentUser': [dbinfo],
        'user': user
    })

@app.route('/rest/addressbook/tree')
@check.authenticated
def fetch_books_tree():
    books_tree = tree.read(HANA_CREDENTIALS)
    return jsonify(books_tree)

@app.route('/rest/addressbook/testdata')
@check.authenticated
def create_sample_data():
    user_info = tokenparser.fetch_user_token(request)[0]

    book.insert_sample_data(HANA_CREDENTIALS, user_info.get('user_name', ''))
    return jsonify({'status': 'success'})

@app.route('/rest/addressbook/testdataDestructor')
@check.authenticated
def delete_table_data():
    book.delete_sample_data(HANA_CREDENTIALS)
    return jsonify({'status': 'success'})

PORT = int(os.getenv("PORT", 9099))

if __name__ == '__main__':
    # Run the app, listening on all IPs with our chosen port number
    app.run(host='0.0.0.0', port=PORT)
