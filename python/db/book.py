""" Database initialization and cleanup """
from random import randint
from db import dbcon

def _insert_book(cursor, userid):
    book_id = randint(1, 100000)
    book_name = 'My Book #' + str(book_id) + ' created by ' + userid

    sql = 'insert into "com.sap.xs2.samples::AddressBook.Book" values (?, ?)'
    cursor.execute(sql, (book_id, book_name))

    return book_id

def _insert_addresses(cursor, bookid):
    sql = 'insert into "com.sap.xs2.samples::AddressBook.Address" \
        values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'

    addressid = randint(1, 100000)
    cursor.execute(sql, (
        addressid,
        bookid,
        'John',
        'Doe',
        'Dietmar-Hopp-Allee 16',
        'Walldorf',
        'Germany',
        '69169',
        '+49 6227 7 12345',
        'john.doe@sap.com',
        'https://sap.de'
    ))

    addressid = randint(1, 100000)
    cursor.execute(sql, (
        addressid,
        bookid,
        'Max',
        'Mustermann',
        'Dietmar-Hopp-Allee 16',
        'Walldorf',
        'Germany',
        '69169',
        '+49 6227 7 54321',
        'john.doe@sap.com',
        'https://sap.de'
    ))

def insert_sample_data(hana_config, userid):
    """ inserts two address books in DB """
    connection = dbcon.create_connection(hana_config)
    cursor = dbcon.set_current_schema(connection.cursor(), hana_config)

    bookid = _insert_book(cursor, userid)
    _insert_addresses(cursor, bookid)

    connection.commit()
    connection.close()

def delete_sample_data(hana_config):
    """ deletes database content """
    connection = dbcon.create_connection(hana_config)
    cursor = dbcon.set_current_schema(connection.cursor(), hana_config)

    cursor.execute('delete from "com.sap.xs2.samples::AddressBook.Book"')
    cursor.execute('delete from "com.sap.xs2.samples::AddressBook.Address"')

    connection.commit()
    connection.close()
