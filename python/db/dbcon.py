""" Utilities to create and initialize HANA DB connection """
from hdbcli import dbapi

def create_connection(hana_config):
    """ creates HANA db connection """
    return dbapi.connect(
        address=hana_config['host'],
        port=int(hana_config['port']),
        user=hana_config['user'],
        password=hana_config['password'])

def set_current_schema(cursor, hana_config):
    """ set schema """
    cursor.execute('SET SCHEMA {schema}'.format(schema=hana_config['schema']))
    return cursor

def set_current_user(cursor, token):
    """ set user """
    cursor.execute("SET 'XS_APPLICATIONUSER' = '" + token + "'")
