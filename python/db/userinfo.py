""" Fetches session information from database like
current user, schema and xs application user """
from db import dbcon

def set_and_fetch_db_user(hana_config, token=None):
    """ set XS_APPLICATIONUSER via token to connection
    and return session info """
    connection = dbcon.create_connection(hana_config)
    cursor = connection.cursor()

    dbcon.set_current_schema(cursor, hana_config)
    dbcon.set_current_user(cursor, token)

    sql = "SELECT TOP 1 CURRENT_USER, SESSION_USER, SESSION_CONTEXT('XS_APPLICATIONUSER') as \
        APPLICATION_USER FROM DUMMY"
    cursor.execute(sql)
    user = cursor.fetchone()

    connection.close()

    return {
        'CURRENT_USER': user[0],
        'SESSION_USER': user[1],
        'APPLICATION_USER': user[2]
    }
