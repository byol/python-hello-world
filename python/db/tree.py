""" Read address books tree """
from db import dbcon


def _get_cursor(connection, hana_config):
    cursor = connection.cursor()
    dbcon.set_current_schema(cursor, hana_config)
    return cursor


def _format_address(address):
    first_name = address[1]
    last_name = address[2]
    name = '{} {}'.format(first_name, last_name)
    return {
        'id': address[0],
        'first_name': first_name,
        'last_name': last_name,
        'phone': address[3],
        'city': address[4],
        'name': name
    }


def _fetch_addresses(cursor, bookid):
    sql = 'select "id", "first_name", "last_name", "phone", "city" from \
        "com.sap.xs2.samples::AddressBook.Address" where "book.id"=?'

    addresses = []
    if cursor.execute(sql, (bookid)):
        for address in cursor:
            addresses.append(_format_address(address))

    return addresses


def _read_books(cursor):
    sql = 'select "id", "name" from "com.sap.xs2.samples::AddressBook.Book"'
    cursor.execute(sql)
    return cursor.fetchall()


def read(hana_config):
    """ all address books from DB """
    connection = dbcon.create_connection(hana_config)
    cursor = _get_cursor(connection, hana_config)

    books = _read_books(cursor)

    root = {}
    for book in books:
        book_id = book[0]
        book_node = {'name': book[1]}

        for address in _fetch_addresses(cursor, book_id):
            address_id = str(address['id'])
            book_node[address_id] = address

        root[book_id] = book_node

    return {'books': root}
