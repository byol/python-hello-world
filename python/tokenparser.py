""" Validate and parse JWT token """

import json
import logging
from flask import g
from sapjwt import jwtValidation


def _parse_jwt(token_str):
    uaa_credentials = g._uaa_credentials # pylint: disable=protected-access

    validator = jwtValidation()
    validator.loadPEM(str(uaa_credentials['verificationkey']))
    validator.checkToken(token_str)

    if validator.getErrorDescription() != "":
        logging.getLogger('/JWTValidation').error(
            "Error in JWT: %s", validator.getErrorDescription())

    payload = validator.getPayload()

    return json.loads(payload) if payload else {}


def fetch_user_token(request):
    """ Fetch and parse bearer token from `Authorization` header """
    auth_header = request.headers.get('Authorization')
    if auth_header is None:
        return None

    encoded_token = auth_header.replace('Bearer ', '')
    return (_parse_jwt(str(encoded_token)), encoded_token)
