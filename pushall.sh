#!/bin/bash

cf create-service hana hdi-shared python-hdi-container && \
cf create-service xsuaa application python-uaa -c xs-security.json

cf push python-hello-world-db && \
cf push python-hello-world-backend && \
cf push python-hello-world

echo 'done.'

cf a | grep hello
