#!/bin/bash

cf d python-hello-world -r -f
cf d python-hello-world-db -r -f
cf d python-hello-world-backend -r -f

cf ds python-hdi-container -f
cf ds python-uaa -f

echo 'done.'
